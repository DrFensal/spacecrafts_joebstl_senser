#ifndef LOSDECISION_H
#define LOSDECISION_H

#include "Decision.h"
#include "Spacecraft.h"

class LOSDecision: public Decision{
protected:
	Spacecraft* mHumanSpacecraft;
	Spacecraft* mAISpacecraft;
public:
	LOSDecision();
	LOSDecision(TreeNode* yesNode, TreeNode* noNode, Spacecraft* humanSpacecraft, Spacecraft* AISpacecraft);
	virtual TreeNode* decide();
	virtual void update(float delta);
};

#endif