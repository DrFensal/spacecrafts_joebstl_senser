 #ifndef __Game_Application_h_
 #define __Game_Application_h_ 

#include "BaseApplication.h"
#include "FileWatcher.h"
#include "FileWatcher.h"
#include "FileWatcherImpl.h"
#include "FileWatcherWin32.h"
#include <string>

using namespace Ogre;
using namespace OgreBulletDynamics;
using namespace OgreBulletCollisions;
using namespace std;

namespace scripting
{
	class Manager;
}

class Spacecraft;
class SpacecraftController;
class HumanController;
class Rocket;
class NavigationGraph;
class GameConfig;
class DebugOverlay;

class GameApplication : public BaseApplication, Ogre::Singleton<GameApplication>, public Ogre::RenderTargetListener
{

	class UpdateListener : public FW::FileWatchListener
	{
		GameApplication& game;
	public:
		UpdateListener(GameApplication& g) : game(g) {}
		void handleFileAction(FW::WatchID watchid, const string& dir, const string& filename,
			FW::Action action)
		{
			switch (action)
			{
			case FW::Actions::Add:
				std::cout << "File (" << dir + "\\" + filename << ") Added! " << std::endl;
				break;
			case FW::Actions::Delete:
				std::cout << "File (" << dir + "\\" + filename << ") Deleted! " << std::endl;
				break;
			case FW::Actions::Modified:
				std::cout << "File (" << dir + "\\" + filename << ") Modified! " << std::endl;
				game.reloadConfig();
				break;
			default:
				std::cout << "Should never happen!" << std::endl;
			}
		}
	};

public:
	enum Mode
	{
		MODE_STANDALONE,
		MODE_SERVER,
		MODE_CLIENT,

		MODE_COUNT
	};

 	GameApplication(Mode mode, String address);
 
 	~GameApplication();

	static GameApplication& getSingleton(void);

    static GameApplication* getSingletonPtr(void);

	/// creates a new rocket at the given position.
	void createRocket(const Vector3& position, const Vector3& direction);

	/// releases a rocket (-> delete the rocket in the next frame)
	void releaseRocket(Rocket* rocket);

	const std::vector<Spacecraft*>& getSpacecrafts() const
	{
		return mSpacecrafts;
	}

	OgreBulletDynamics::DynamicsWorld* getPhysics()
	{
		return mWorld;
	}

	void reloadConfig();
 
 protected:
    virtual bool configure(void);

	virtual void createScene(void);

	virtual void createFrameListener(void);
	virtual void preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
	virtual void postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);

	virtual void createSpacecrafts(void);

	virtual void createCamera(void);

	virtual void createWalls(void);

	void createDynamicWorld(Vector3 &gravityVector, AxisAlignedBox &bounds);

	virtual bool frameStarted(const Ogre::FrameEvent& evt);
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);
	virtual bool frameEnded(const Ogre::FrameEvent& evt);

	virtual bool keyPressed(const OIS::KeyEvent &arg);
    virtual bool keyReleased(const OIS::KeyEvent &arg);

private:
	/// OgreBullet World
 	OgreBulletDynamics::DynamicsWorld* mWorld;	

	// DebugDrawer for the physics world
 	OgreBulletCollisions::DebugDrawer* mDebugDrawer;	

	// Util for text debug rendering.
	DebugOverlay* mDebugOverlay;

	GameConfig* mGameConfig;

	std::vector<Spacecraft*> mSpacecrafts;
	std::vector<SpacecraftController*> mControllers;
	std::list<Rocket*> mRockets;
	std::list<Rocket*> mReleasedRockets;

	HumanController* mHumanController;
	int mRocketCounter;

	scripting::Manager* mScriptingManager;

	bool mShowDebugDraw;
	bool mShowNavigationGraph;
	bool mFollowPlayerCam;
	float mSynchTimer;

	FW::FileWatcher* fileWatcher = new FW::FileWatcher();
	FW::WatchID watchid;

	Mode mMode;
	String mAddress;

	Ogre::TexturePtr rtt_texture;
	Ogre::RenderTexture *renderTexture;
	Ogre::Rectangle2D *mMiniScreen;
	Ogre::SceneNode* miniScreenNode;
	Ogre::MaterialPtr renderMaterial;

	void update(float delta);
 };

#endif
