#ifndef ACTION_H
#define ACTION_H

#include "TreeNode.h"

class Action: public TreeNode{
public:
	virtual TreeNode* decide();
	virtual void update(float delta);
};

#endif