#ifndef PATROLACTION_H
#define PATROLACTION_H

#include "Action.h"
#include "Spacecraft.h"
#include "SpacecraftController.h"
#include "Path.h"

class PatrolAction : public Action{
protected:
	Spacecraft* mSpacecraft;
	SpacecraftController* mController;
	float mCurrentParam;
	Path mPath;
	Path mPatrolPath;
public:
	PatrolAction();
	PatrolAction(Spacecraft* spacecraft, SpacecraftController* controller, Path path);
	virtual TreeNode* decide();
	void update(float delta);
};

#endif