#ifndef FOLLOWACTION_H
#define FOLLOWACTION_H

#include "Action.h"
#include "Spacecraft.h"
#include "SpacecraftController.h"
#include "Path.h"

class FollowAction : public Action{
protected:
	Spacecraft* mSpacecraft;
	Spacecraft* mHumanSpacecraft;
	SpacecraftController* mController;
	float mCurrentParam;
	Path mPath;
	Path mPatrolPath;
public:
	FollowAction();
	FollowAction(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, SpacecraftController* controller, Path path);
	virtual TreeNode* decide();
	void update(float delta);
};

#endif