 #ifndef __AIController_h_
 #define __AIController_h_ 

#include "SpacecraftController.h"
#include "Path.h"
#include "FollowAction.h"
#include "PatrolAction.h"
#include "LOSDecision.h"


class AIController: public SpacecraftController
{
public:
	AIController(Spacecraft* spacecraft, Spacecraft* mHumanSpacecraft, const Path& patrol);

	virtual void update(float delta);

private:
	Spacecraft* mHumanSpacecraft;

	Path mPath;
	Path mPatrolPath;
	float mCurrentParam;
	TreeNode* mCurrentAction;

	LOSDecision mRoot;
	FollowAction mFollowAction;
	PatrolAction mPatrolAction;
	
};


#endif