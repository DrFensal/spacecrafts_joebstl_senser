#ifndef TREENODE_H
#define TREENODE_H

class TreeNode{
public:
	virtual TreeNode* decide() = 0;
	virtual void update(float delta) = 0;
};

#endif