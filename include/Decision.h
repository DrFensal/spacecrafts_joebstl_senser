#ifndef DECISION_H
#define DECISION_H

#include "TreeNode.h"

class Decision: public TreeNode{
protected:
	TreeNode* yesNode;
	TreeNode* noNode;
public:
	virtual TreeNode* decide();
	virtual void update(float delta);
};

#endif