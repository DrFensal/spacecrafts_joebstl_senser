#include "PatrolAction.h"

PatrolAction::PatrolAction(){

}

PatrolAction::PatrolAction(Spacecraft* spacecraft, SpacecraftController* controller, Path path){
	mSpacecraft = spacecraft;
	mController = controller;
	mCurrentParam = 0.0f;
	mPath = path;
	mPatrolPath = path;
}

TreeNode* PatrolAction::decide(){
	return this;
}

void PatrolAction::update(float delta){
	Ogre::Vector3 linearSteering = mController->obstacleAvoidance() + mController->wallAvoidance() + mController->followPath(mPath, mCurrentParam);
	mPath = Path(mPatrolPath);
	mSpacecraft->setSteeringCommand(linearSteering);
}