#include "FollowAction.h"
#include "NavigationGraph.h"

FollowAction::FollowAction(){

}

FollowAction::FollowAction(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, SpacecraftController* controller, Path path){
	mSpacecraft = spacecraft;
	mHumanSpacecraft = humanSpacecraft;
	mController = controller;
	mCurrentParam = 0.0f;
	mPath = path;
	mPatrolPath = path;
}

TreeNode* FollowAction::decide(){
	return this;
}

void FollowAction::update(float delta){
	Ogre::Vector3 linearSteering = mController->obstacleAvoidance() + mController->wallAvoidance() + mController->followPath(mPath, mCurrentParam);
	std::vector<Ogre::Vector3> waypoints = NavigationGraph::getSingleton().calcPath(mSpacecraft->getPosition(), mHumanSpacecraft->getPosition());

	if (waypoints.size() > 1)
		mPath = Path(waypoints, Path::PATH_NORMAL);

	linearSteering = mController->obstacleAvoidance() + mController->wallAvoidance() + mController->followPath(mPath, mCurrentParam);
	mSpacecraft->setSteeringCommand(linearSteering);
}