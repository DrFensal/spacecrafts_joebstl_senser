#include "LOSDecision.h"
#include "GameConfig.h"
#include "WorldUtil.h"
#include <OgreVector3.h>

LOSDecision::LOSDecision(){

}

LOSDecision::LOSDecision(TreeNode* yesNode, TreeNode* noNode, Spacecraft* humanSpaceCraft, Spacecraft* AISpacecraft){
	this->yesNode = yesNode;
	this->noNode = noNode;
	this->mHumanSpacecraft = humanSpaceCraft;
	this->mAISpacecraft = AISpacecraft;
}

TreeNode* LOSDecision::decide(){
	Ogre::Vector3 colPoint;
	Ogre::Vector3 colNormal;

	//buggy
	/*if (WorldUtil::rayCast(mAISpacecraft, 10000.0f, colPoint, colNormal)){
		if ((mHumanSpaceCraft->getPosition() - colPoint).length() <= mHumanSpaceCraft->getRadius()){
			return yesNode;
		}
		else{
			return noNode;
		}

	}*/

	if (mHumanSpacecraft->getPosition().distance(mAISpacecraft->getPosition()) <100){
		return yesNode->decide();
	}
	else{
		return noNode->decide();
	}

	if (mHumanSpacecraft->getPosition().distance(mAISpacecraft->getPosition()) < 100){
		return yesNode;
	}
	else{
		return noNode;
	}
}

void LOSDecision::update(float delta){

}