#include "stdafx.h"

#include <stack>
#include "NavigationGraph.h"
#include "NavigationNode.h"
#include "Connection.h"
#include "PathfindingList.h"
#include "NavigationGraphDebugDisplay.h"
#include "DebugDisplay.h"
#include "WorldUtil.h"

using namespace Ogre;
using namespace OgreBulletDynamics;
using namespace OgreBulletCollisions;

template<> NavigationGraph* Ogre::Singleton<NavigationGraph>::msSingleton = 0;


NavigationGraph::NavigationGraph(SceneManager* sceneMgr, int _x, int _z, int _width, int _height):
	origin((float) _x, 0.0f, (float) _z),
	width(_width),
	height(_height)
{
	mDebugDisplay = new NavigationGraphDebugDisplay(sceneMgr, 0.5f);
	gridWidth = (int) floor(width / NavigationNode::NODE_SIZE);
	gridDepth = (int) floor(height / NavigationNode::NODE_SIZE);
	
	int gridSize = gridWidth * gridDepth;
	grid.reserve(gridSize);
	
	for (int i = 0; i < gridSize; i++)
	{
		grid.push_back(NULL);
	}
}


NavigationGraph::~NavigationGraph()
{
	for (size_t i = 0; i < grid.size(); i++)
	{
		delete grid[i];
	}

	grid.clear();
}

NavigationGraph* NavigationGraph::getSingletonPtr(void)
{
    return msSingleton;
}
NavigationGraph& NavigationGraph::getSingleton(void)
{  
    assert( msSingleton );  return ( *msSingleton );  
}

void NavigationGraph::setDebugDisplayEnabled(bool enable)
{
	mDebugDisplay->setEnabled(enable);
}

bool NavigationGraph::checkSpaceForNode(OgreBulletDynamics::DynamicsWorld* world, const Vector3& position) const
{
	const Vector3 offset[] = { 
		Vector3(-NavigationNode::NODE_SIZE_HALF*0.5f, 0.0f, 0.0f), 
		Vector3(NavigationNode::NODE_SIZE_HALF*0.5f, 0.0f, 0.0f), 
		Vector3(0.0f, 0.0f, -NavigationNode::NODE_SIZE_HALF*0.5f), 
		Vector3(0.0f, 0.0f, NavigationNode::NODE_SIZE_HALF*0.5f) 
	};

	int blocked = 0;

	// send multiple rays to check if spot is free.
	for (int i = 0; i < 4; i++)
	{
		btVector3 start(position.x + offset[i].x, 100.0f, position.z + offset[i].z);
		btVector3 end(position.x + offset[i].x, 0.0f, position.z + offset[i].z);
			
		btCollisionWorld::ClosestRayResultCallback rayCallback(start, end);
 
		// Perform raycast
		world->getBulletCollisionWorld()->rayTest(start, end, rayCallback);
 
		if(rayCallback.hasHit()) 
		{
			return false;
		}
	}

	return true;
}

bool NavigationGraph::rayTest(OgreBulletDynamics::DynamicsWorld* world, const Vector3& start, const Vector3& end) const
{
	btVector3 startBt = OgreBtConverter::to(start);
	btVector3 endBt = OgreBtConverter::to(end);

	btCollisionWorld::ClosestRayResultCallback rayCallback(startBt, endBt);
 
	// Perform raycast
	world->getBulletCollisionWorld()->rayTest(startBt, endBt, rayCallback);
 
	return rayCallback.hasHit();
}


void NavigationGraph::calcGraph(OgreBulletDynamics::DynamicsWorld* world)
{
	for (int z = 0; z < gridDepth; z++)
	{
		for (int x = 0; x < gridWidth; x++)
		{
			Vector3 pos((float) (x * NavigationNode::NODE_SIZE), 5.0f,
					 (float) (z * NavigationNode::NODE_SIZE));

			pos += origin;

			if(!checkSpaceForNode(world, pos))
			{
				continue;
			}
			
			NavigationNode* node = new NavigationNode(Vector3((float) x, 0.0f, (float) z), pos);
			NavigationNode* leftNode = getNode(x-1, z);
			NavigationNode* topNode = getNode(x, z-1);
			
			if (leftNode != NULL && !rayTest(world, leftNode->center, node->center))
			{
				Vector3 distance = node->center - leftNode->center;
				float cost = distance.length();
				node->addConnection(Connection(node, leftNode, cost));
				leftNode->addConnection(Connection(leftNode, node, cost));
			}
			if (topNode != NULL && !rayTest(world, topNode->center, node->center))
			{
				Vector3 distance = node->center - topNode->center;
				float cost = distance.length();
				node->addConnection(Connection(node, topNode, cost));
				topNode->addConnection(Connection(topNode, node, cost));
			}

			grid[x + z * gridWidth] = node;
		}
	}

	debugDraw();
}

template<class T>
static T round(T r) 
{
	return (r > (T) 0.0) ? floor(r + (T) 0.5) : ceil(r - (T) 0.5);
}

NavigationNode* NavigationGraph::getNodeAt(const Vector3& position)
{
	int idxX = (int) round((position.x - origin.x) / NavigationNode::NODE_SIZE);
	int idxZ = (int) round((position.z - origin.z) / NavigationNode::NODE_SIZE);
	
	return getNode(idxX, idxZ);
}

NavigationNode* NavigationGraph::getNearestNode(const Vector3& position)
{
	int idxX = (int)round((position.x - origin.x) / NavigationNode::NODE_SIZE);
	int idxZ = (int)round((position.z - origin.z) / NavigationNode::NODE_SIZE);

	Vector2 start(idxX, idxZ);

	static const int MAX_DEPTH = 100;
	static const int OFFSET_LENGHT = 4;
	static const Vector2 OFFSET[OFFSET_LENGHT] = { Vector2(0, 1), Vector2(1, 0), Vector2(0, -1), Vector2(-1, 0) };

	std::deque<Vector2> open;
	std::deque<Vector2> closed;

	open.push_back(start);

	int depth = 0;

	while (open.size() && (depth++ < MAX_DEPTH))
	{
		Vector2 current = open.front();
		open.pop_front();

		NavigationNode* node = getNode(current.x, current.y);

		Vector3 collPoint, collNormal;

		if (node != NULL && !WorldUtil::rayCast(position, node->getCenter(), collPoint, collNormal))
		{
			return node;
		}

		for (int i = 0; i < OFFSET_LENGHT; i++)
		{
			Vector2 neighbor = current + OFFSET[i];

			if ((std::find(open.begin(), open.end(), neighbor) == open.end()) &&
				(std::find(closed.begin(), closed.end(), neighbor) == closed.end()))
			{
				open.push_back(neighbor);
			}
		}

		closed.push_back(current);
		depth++;
	}

	return NULL;
}

NavigationNode* NavigationGraph::getNode(int idxX, int idxZ)
{
	if ((idxX >= 0) && (idxX < gridWidth) && (idxZ >= 0) && (idxZ < gridDepth))
	{
		return grid[idxX + idxZ * gridWidth];
	}
	else
	{
		return NULL;
	}
}

void NavigationGraph::debugDraw() const
{
	for (size_t i = 0; i < grid.size(); i++)
	{
		if (grid[i] != NULL)
		{
			grid[i]->debugDraw(mDebugDisplay);
		}
	}

	mDebugDisplay->build();
}

std::vector<Vector3> NavigationGraph::calcPath(const Vector3& currentPosition, const Vector3& targetPosition)
{
	NavigationNode* start = getNodeAt(currentPosition);
	NavigationNode* goal = getNodeAt(targetPosition);

	std::vector<Vector3> path;

	if (start == NULL || goal == NULL)
	{
		return path;
	}

	PathfindingList openList;
	PathfindingList closedList;
	PathfindingList::NodeRecord current;
	
	PathfindingList::NodeRecord startRecord;
	startRecord.node = start;
	startRecord.estimatedTotalCost = start->getCenter().distance(goal->getCenter());
	startRecord.costSoFar = 0;

	openList.add(startRecord);

	while (!openList.isEmpty())
	{
		current = openList.getSmallest();

		//stop if we reach the end
		if (current.node == goal)
		{
			break;
		}

		//iterate through all neighbors, calculate the costs and add them to the Open list
		std::vector<Connection> neighbors = current.node->getConnections();
		std::vector<Connection>::iterator it;

		for (std::vector<Connection>::iterator it = neighbors.begin(); it != neighbors.end(); ++it)
		{
			if (!closedList.contains(it->getToNode()))
			{
				PathfindingList::NodeRecord n;

				//if node is not in open list, calculate the costs and set the connection and put it on the open list
				if (!openList.contains(it->getToNode()))
				{
					n.node = it->getToNode();

					n.connection = *it;

					n.costSoFar = current.costSoFar + it->getCost();
					n.estimatedTotalCost = n.costSoFar + it->getFromNode()->getCenter().distance(it->getToNode()->getCenter());

					openList.add(n);
				}
				//if node is already in open list, see if cost of current path(node) is cheaper and if so, recalculate the costs and change the connection
				else
				{
					n.costSoFar = current.costSoFar + it->getCost();
					n.estimatedTotalCost = n.costSoFar + it->getFromNode()->getCenter().distance(it->getToNode()->getCenter());
					if (n.estimatedTotalCost < openList.find(it->getToNode()).estimatedTotalCost)
					{
						openList.remove(it->getToNode());
						n.connection = *it;
						n.node = it->getToNode();

						openList.add(n);
					}
				}
				
			}
			
		}


		openList.remove(current.node);
		closedList.add(current);
	}

	if (current.node == start)
	{
		path.push_back(start->getCenter());
	}
		
	

	while (current.node != start)
	{
		openList;
		closedList;
		closedList.find(current.connection.getFromNode());
		path.push_back(current.node->getCenter());
		current = closedList.find(current.connection.getFromNode());
	}
	
	if (path.size() < 2)
		path.push_back(goal->getCenter());
	
	return path;

}
