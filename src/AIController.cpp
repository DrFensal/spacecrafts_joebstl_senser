#include "StdAfx.h"

#include <algorithm> 
#include "AIController.h"
#include "DebugOverlay.h"

#include "DebugDisplay.h"

#include "NavigationGraph.h"
#include "GameConfig.h"
#include "GameApplication.h"
#include "MathUtil.h"

using namespace Ogre;

AIController::AIController(Spacecraft* spacecraft, Spacecraft* humanSpacecraft, const Path& patrol):
	SpacecraftController(spacecraft),
	mHumanSpacecraft(humanSpacecraft),
	mCurrentParam(0.0f),
	mPath(patrol),
	mPatrolPath(patrol)
{
	mFollowAction = FollowAction(spacecraft, humanSpacecraft, this, patrol);
	mPatrolAction = PatrolAction(spacecraft, this, patrol);
	mRoot = LOSDecision(&mFollowAction, &mPatrolAction, humanSpacecraft, spacecraft);
	mCurrentAction = &mPatrolAction;
}

void AIController::update(float delta)
{
	/*Vector3 linearSteering = obstacleAvoidance() + wallAvoidance() + followPath(mPath, mCurrentParam);

	if (mHumanSpacecraft->getPosition().distance(mSpacecraft->getPosition()) < 200)
	{
		std::vector<Vector3> waypoints = NavigationGraph::getSingleton().calcPath(mSpacecraft->getPosition(), mHumanSpacecraft->getPosition());

		if (waypoints.size() > 1)
			mPath = Path(waypoints, Path::PATH_NORMAL);

		linearSteering = obstacleAvoidance() + wallAvoidance() + followPath(mPath, mCurrentParam);
	}
	else
	{
		mPath = Path(mPatrolPath);
	}
	

	getSpacecraft()->setSteeringCommand(linearSteering);*/
	mCurrentAction = mRoot.decide();
	mCurrentAction->update(delta);

}


 
