#version 120

uniform	mat4 worldViewProj;

attribute vec4 vertex;
attribute vec3 normal;
attribute vec4 uv0;

varying	vec2 oUv;

void main() 
{
	oUv = uv0.xy;
	gl_Position = worldViewProj * vertex;
}