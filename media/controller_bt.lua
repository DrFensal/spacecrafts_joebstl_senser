class 'Behavior'

STATUS_INVALID = 0;
STATUS_RUNNING = 1;
STATUS_SUCCESS = 2;
STATUS_FAILURE = 3;
STATUS_NAMES = {[STATUS_INVALID] = "STATUS_INVALID", [STATUS_RUNNING] = "STATUS_RUNNING", [STATUS_SUCCESS] = "STATUS_SUCCESS", [STATUS_FAILURE] = "STATUS_FAILURE"}


function Behavior:__init()
	self.name = "Behavior"
	self.status = STATUS_INVALID
end

function Behavior:onInitialized()

end

function Behavior:onUpdate(delta)
	return STATUS_SUCCESS
end

function Behavior:onTerminated()

end

function Behavior:printTree(space)
	log(space .. self.name .. " status: " .. STATUS_NAMES[self.status])
end

function Behavior:reset()
	status = STATUS_INVALID
end


function Behavior:tick(delta)
	if self.status == STATUS_INVALID then
		self:onInitialized()
	end

	self.status = self:onUpdate(delta)
	
	if self.status ~= STATUS_RUNNING then
		self:onTerminated()
	end
	
	return self.status
end

class 'Composite' (Behavior)

function Composite:__init() super()
	self.name = "Composite"
	self.children = {}
end

function Composite:add(child)
	table.insert(self.children, child);
end

function Composite:printTree(space)
	Behavior.printTree(self, space)
	
	for k,v in pairs(self.children) do
		v:printTree(space .. " ")
	end
end


class 'Sequence' (Composite)

function Sequence:__init() super()
	self.name = "Sequence"
end

function Sequence:onInitialized()
	self.currentChild = 1
end

function Sequence:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	for i = self.currentChild, #self.children do
		local child = self.children[i];
		
		local s = child:tick(delta);
		
		if s ~= STATUS_SUCCESS then
			return s
		end

		self.currentChild = self.currentChild + 1
		
		if self.currentChild > #self.children then
			self.currentChild = #self.children
			return STATUS_SUCCESS
		end
	end
	
	return STATUS_INVALID
end

class 'Selector' (Composite)

function Selector:__init() super()
	self.name = "Selector"
end

function Selector:onInitialized()
	self.currentChild = 1
end

function Selector:onUpdate(delta)
	if  #self.children == 0 then
		return STATUS_FAILURE
	end

	for i = self.currentChild, #self.children do
		local child = self.children[i];
		
		local s = child:tick(delta);
		
		if s ~= STATUS_FAILURE then
			return s
		end

		self.currentChild = self.currentChild + 1
		
		if self.currentChild > #self.children then
			self.currentChild = #self.children
			return STATUS_FAILURE
		end
	end
	
	return STATUS_INVALID
end


class 'Action' (Behavior)

function Action:__init(controller) super()
	self.controller = controller
	self.name = "Action"
	
end

function Action:onUpdate(delta)
	return STATUS_RUNNING
end

class 'Condition' (Behavior)

function Condition:__init() super()
	self.name = "Condition"
end

function Condition:testCondition()
	return false
end
	
function Condition:onUpdate()
	if self:testCondition() then
		return STATUS_SUCCESS
	else
		return STATUS_FAILURE
	end
end



init = function(controller)
	local root = Sequence()
	return root;
end


update = function(controller)
end


