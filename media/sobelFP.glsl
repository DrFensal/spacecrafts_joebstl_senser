#version 120

uniform	sampler2D base;

varying	vec2 oUv;

vec4 sobel(){
	float x=float(1.0/1024.0);
	float y=float(1.0/768.0);
	int edgestrength = 10;
	
	vec4 p1= texture2D(base, vec2(oUv.x-x, oUv.y-y));
	vec4 p2= texture2D(base, vec2(oUv.x, oUv.y-y));
	vec4 p3= texture2D(base, vec2(oUv.x+x, oUv.y-y));
	
	vec4 p4= texture2D(base, vec2(oUv.x-x, oUv.y));
	vec4 p5= texture2D(base, vec2(oUv.x, oUv.y));
	vec4 p6= texture2D(base, vec2(oUv.x+x, oUv.y));
	
	vec4 p7= texture2D(base, vec2(oUv.x-x, oUv.y+y));
	vec4 p8= texture2D(base, vec2(oUv.x, oUv.y+y));
	vec4 p9= texture2D(base, vec2(oUv.x+x, oUv.y+y));
	
	vec4 sobel = vec4(vec4(1.0).xyz-clamp((p1+(p2+p2)+p3-p7-(p8+p8)-p9)+(p3+(p6+p6)+p9-p1-(p4+p4)-p7)*edgestrength, 0, 1).xyz, 1.0);
	float color = abs((sobel.x+sobel.y+sobel.z)/3);
	return vec4(color, color, color, 1.0);
}

void main() 
{
	vec4 color1=texture2D(base, oUv);
	vec4 color2=sobel();
	
	gl_FragColor = color1*color2;
	//gl_FragColor = vec4(1.0, 0.0, 0.0, 0.0);
}